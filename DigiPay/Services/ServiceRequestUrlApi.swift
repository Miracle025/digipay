//
//  ServiceRequestUrlApi.swift
//  DigiPay
//
//  Created by Fateme Mohammadi on 11/1/19.
//  Copyright © 2019 Fateme Mohammadi. All rights reserved.
//

import Foundation

struct ServiceRequestUrlApi {

static let Shared = ServiceRequestUrlApi()


// MARK: Define all url path

    static var getTrack: URL {
//        return URL(string: ServiceRequestDomainHelper.Shared.DomainV1.value + "/search")!
        return URL(string: "https://api.spotify.com/v1" + "/search?q=magic&type=track")!
        
    }

}

//
//  ServiceRequestDomainHelper.swift
//  DigiPay
//
//  Created by Fateme Mohammadi on 11/1/19.
//  Copyright © 2019 Fateme Mohammadi. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import ObjectMapper
import Toast_Swift

struct ServiceRequestDomainHelper {
    
    // MARK: - Properties
    private var debugMode: Bool {
        #if DEBUG
        return true
        #else
        return false
        #endif
    }

    static let Shared = ServiceRequestDomainHelper()
    var disposeBag = DisposeBag()
    var ipModel: Variable<IpCheckModel?> = Variable(nil)
    var responseStatus: Variable<ResponseStatus> = Variable(.unknown)
    var DomainV1: Variable<String> = Variable("")
    
    
    // MARK: - Init
    
    init() {
        ipHandler()
    }
    
    
    private func ipHandler() {
        if debugMode{
           setDebugServer()
        }else{
           setReleaseServer()
        }
    }
    
    private func setDebugServer() {
        ipModel.asObservable()
            .filter({ $0 != nil })
            .map({ $0?.country })
            .filter({ $0 != nil })
            .subscribeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { countryIdentifier in
                self.set_BetaServer()
            })
            .disposed(by: disposeBag)
    }
    
    private func setReleaseServer() {
        ipModel.asObservable()
            .filter({ $0 != nil })
            .map({ $0?.country })
            .filter({ $0 != nil })
            .subscribeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { countryIdentifier in
                    self.set_ReleaseServer()
            })
            .disposed(by: disposeBag)
    }
 
    
    // MARK: - Release
    func set_ReleaseServer() {
        self.DomainV1.value = DomainIPs.baseUrl.rawValue + DomainPortsRelease.V1.rawValue + DomainPortsRelease.V1.rawValue
    }
    
    
    // MARK: - Development
    
    func set_BetaServer() {
        self.DomainV1.value = DomainIPs.baseUrl.rawValue + DomainPortsDevelopment.V1.rawValue + DomainPortsRelease.V1.rawValue
    }
    
}

// MARK: - Set Url

enum DomainIPs: String {
     case baseUrl = "https://api.spotify.com/"
}

////  For release servers
enum DomainPortsRelease: String {
    case V1 = "v1"
}

/// For development servers
enum DomainPortsDevelopment: String {
    case V1 = ":8081"
}


enum ApiVersioning: String {
    case V1 = "/api/v1.0/"
}

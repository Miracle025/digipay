//
//  Network.swift
//  Storm
//
//  Created by Mohammad Zakizadeh on 7/22/18.
//  Copyright © 2018 Storm. All rights reserved.
//
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

private class ApiModel<T, B: BaseMappable>: Mappable {
    
    var success: Bool = false
    var data: Any?
    var error: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        error   <- map["error"]
        data    <- map["data"]
        
        if success, data != nil {
            if T.self is Array<B>.Type {
                data = Mapper<B>().mapArray(JSONArray: data as! [[String : Any]])
            } else {
                if let temp = Mapper<B>().map(JSONObject: data as! [String: Any]) {
                    data = temp
                }
            }
        }
        
        if success, B.self is Empty.Type {
            data = Empty()
        }
    }
    
    func getData() -> T {
        return data as! T
    }
}

class Empty: NSObject, Mappable {
    
    override init(){
        super.init()
    }
    
    convenience required init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map){
        
    }
}

class APIManager {
    
    class var sharedManager : APIManager {
        struct Instance {
            static var instance : APIManager!
        }
        
        if Instance.instance == nil {
            Instance.instance = APIManager()
        }
        
        return Instance.instance
    }
    
    internal static let HEADER_CONTENT_TYPE = "Content-Type"
    internal static let HEADER_AUTHORIZATION = "Authorization"
    internal var CONTENT_TYPE_APPLICATION_JSON = "application/json"
    private let sessionManager: SessionManager
    private var header: HTTPHeaders = [:]
    let token : String
    private static let ERROR_SERVER_STATUS = "Please check network"
    
    
    
    lazy var operationQueue:OperationQueue = {
           let queue = OperationQueue()
           let name = "com.applr.apihelper" + String(Date().timeIntervalSince1970)
           queue.name = name
           queue.qualityOfService = QualityOfService.background
           queue.maxConcurrentOperationCount = 5
           return queue
       }()
    
    init(){
        token = "BQCz0peoSXUIkafmJN7ZQnRfnB3SMbXPVPDoIovV1Ul3JDvFowd_YUtHCjwdbuOVjza13t_S9XFAlW4AY58Zcg1-JmhFgxM_HzYepkd97cZdfdQKFHe8PRqJsynOQWN0m7X-0uLf0GRYjga_FacHbackhAdEgetxOnV4OQ3QYCRsbm-yzJcBTuJCI-_oinu-zUL9u0BuEfU0bl87Lyl1vnMQdQxZx9xiO1PaSjs"
//        token = (UserDefaults.standard.string(forKey: UserDefaultsKeys.userToken)!)
        header =  ["Accept": "application/json", "Content-Type": "application/json", "Authorization": "Bearer \(token)"]
        
        let configuration = URLSessionConfiguration.default
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    private func baseRequest<T,B:BaseMappable>(url: String, method: HTTPMethod, parameters: Parameters?, success: ((ApiModel<T,B>) -> Void)!, failure: ((String) -> Void)!) {
        let startTime = CFAbsoluteTimeGetCurrent()
        NSLog("--> ApiManager :: [\(method)]\(url)")
        
        guard let temp = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {
            print("We could not find the url so we cannot proceed to make request!")
            failure("We could not find the url so we cannot proceed to make request!"/*ApiHelper.ERROR_SERVER_STATUS*/)
            return
        }
        
        Alamofire.request(url , method: method, parameters: parameters,encoding: JSONEncoding.default, headers: header).responseJSON { response in

            switch response.result {
            case .success:
                print("get response from server for your history\(response)")
                do {
                    let json = try JSON(data: response.data!)
                    print(json)
                }catch{
                    print(error)
                }
                break
            case .failure(let error):
                print(error)

            }
        }
    
    }
    
    private func sendRequest<T,B:BaseMappable>(url: String, method: HTTPMethod, parameters: Parameters?, type: B.Type, success: ((T) -> Void)!, failure: ((String) -> Void)!) {
        operationQueue.addOperation {
            self.baseRequest(url:url, method: method, parameters: parameters, success: {(model: ApiModel<T, B>) in success(model.getData())}, failure: failure)
        }
    }
    
    func  fetchSearchTrack(
        onSuccess: @escaping(_ liveFollowings: [Track]) -> Void,
        onError: @escaping(_ errorMessage: String) -> Void
        ) {
        let headers = header
        searchTrack(url: ServiceRequestUrlApi.getTrack, headers: headers, onSuccess: onSuccess, onError: onError)
    }

    
    private func searchTrack(url: URL,
                             headers: [String:String],
                             onSuccess: @escaping(_ topSellers: [Track]) -> Void,
                             onError: @escaping(_ errorMessage: String
        ) -> Void) {
    
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            /// Parse tehe JSON manually ( You can use SwiftJSON, Codable etc ) I just decided to do it manually for simplicity.
            if let json = response.result.value as? [String: Any] {
                /// Get the data array
                if let tracksJSONArray = json["tracks"] as? [String: Any] {
                    var fetchedTracks: [Track] = []
                    if let itemsJSONArray = tracksJSONArray["items"] as? [[String: Any]] {
                        /// Get a single dictionary for each broadCast data
                        for dict in itemsJSONArray {

                            /// Convert each single dictionary to app model.
                            guard let TrackData = Track(JSON: dict) else {
                                print("We couldn't auto transform our friends broadcasts data using object mapper.")
                                return
                            }

                            if !fetchedTracks.contains(TrackData) {
                                fetchedTracks.append(TrackData)
                            }
                        }

                    }
                
                    onSuccess(fetchedTracks)
                    
                } else {
                    onError("We couldn't get list of Tracks from data")
                }
            }else {
                onError("The normal tracks data could not be parsed from the server response")
            }
        }
        
    }
}

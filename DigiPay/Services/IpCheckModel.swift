//
//  IpCheckModel.swift
//  DigiPay
//
//  Created by Fateme Mohammadi on 11/1/19.
//  Copyright © 2019 Fateme Mohammadi. All rights reserved.
//

import Foundation
import ObjectMapper

public final class IpCheckModel: Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let city = "city"
        static let query = "query"
        static let org = "org"
        static let lat = "latitude"
        static let regionName = "region_name"
        static let status = "status"
        static let zip = "zip_code"
        static let countryCode = "country_code"
        static let lon = "longitude"
        static let isp = "isp"
        static let region = "region"
        static let timezone = "time_zone"
        static let country = "country_name"
    }
    
    // MARK: Properties
    public var city: String?
    public var query: String?
    public var org: String?
    public var lat: Float?
    public var regionName: String?
    public var status: String?
    public var zip: String?
    public var countryCode: String?
    public var lon: Float?
    public var isp: String?
    public var region: String?
    public var timezone: String?
    public var country: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        city <- map[SerializationKeys.city]
        query <- map[SerializationKeys.query]
        org <- map[SerializationKeys.org]
        lat <- map[SerializationKeys.lat]
        regionName <- map[SerializationKeys.regionName]
        status <- map[SerializationKeys.status]
        zip <- map[SerializationKeys.zip]
        countryCode <- map[SerializationKeys.countryCode]
        lon <- map[SerializationKeys.lon]
        isp <- map[SerializationKeys.isp]
        region <- map[SerializationKeys.region]
        timezone <- map[SerializationKeys.timezone]
        country <- map[SerializationKeys.country]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = city { dictionary[SerializationKeys.city] = value }
        if let value = query { dictionary[SerializationKeys.query] = value }
        if let value = org { dictionary[SerializationKeys.org] = value }
        if let value = lat { dictionary[SerializationKeys.lat] = value }
        if let value = regionName { dictionary[SerializationKeys.regionName] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = zip { dictionary[SerializationKeys.zip] = value }
        if let value = countryCode { dictionary[SerializationKeys.countryCode] = value }
        if let value = lon { dictionary[SerializationKeys.lon] = value }
        if let value = isp { dictionary[SerializationKeys.isp] = value }
        if let value = region { dictionary[SerializationKeys.region] = value }
        if let value = timezone { dictionary[SerializationKeys.timezone] = value }
        if let value = country { dictionary[SerializationKeys.country] = value }
        return dictionary
    }
    
}

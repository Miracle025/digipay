//
//  AppDelegate.swift
//  DigiPay
//
//  Created by Fateme Mohammadi on 11/1/19.
//  Copyright © 2019 Fateme Mohammadi. All rights reserved.
//

import UIKit
import SpotifyLogin

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let redirectURL: URL = URL(string: "dpg://mydigipay/")!
        SpotifyLogin.shared.configure(clientID: "ba05b9cd59634cefa8493ac961d76ed6",
                                      clientSecret: "80b7235a88264654a105a989f6775a59",
                                      redirectURL: redirectURL)
        
        return true
    }
    
    //Authorization Spotify
    func application(_ app: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        print("This is url that open \(url)")
        let handled = SpotifyLogin.shared.applicationOpenURL(url) { _ in }
        return handled
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}


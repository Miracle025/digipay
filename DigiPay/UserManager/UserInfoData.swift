//
//  UserInfoData.swift
//  Weegle
//
//  Created by mokjinook on 2018. 10. 7..
//  Copyright © 2018년 ojworld. All rights reserved.
//

import Foundation
import ObjectMapper


@objc class UserInfoData: NSObject, Mappable {
    var userId: String = ""
    var auth: String = ""
    

    override init() {
        super.init()
    }

    convenience required init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        userId <- map["user_id"]
        auth <- map["auth"]
    }
}





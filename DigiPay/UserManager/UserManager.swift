//
//  UserManager.swift
//  DigiPay
//
//  Created by Fateme Mohammadi on 11/2/19.
//  Copyright © 2019 Fateme Mohammadi. All rights reserved.
//

class UserManager {

    public static let sharedManager = UserManager()
    private init() {}

    var user: UserInfoData = UserInfoData()

    var authorizationToken: String {
        return user.auth
    }
}

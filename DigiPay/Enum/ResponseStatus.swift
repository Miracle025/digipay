//
//  ResponseStatus.swift
//  DigiPay
//
//  Created by Fateme Mohammadi on 11/1/19.
//  Copyright © 2019 Fateme Mohammadi. All rights reserved.
//

import Foundation

enum ResponseStatus {
    case failed
    case success
    case unknown
    case proccessing
}

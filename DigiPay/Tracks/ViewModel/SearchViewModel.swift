//
//  HomeViewModel.swift
//  Storm
//
//  Created by Mohammad Zakizadeh on 7/17/18.
//  Copyright © 2018 Storm. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa



class SearchViewModel {
    
    public enum SearchError {
        case internetError(String)
        case serverMessage(String)
    }
    
    public let tracks : PublishSubject<[Track]> = PublishSubject()
    public let loading: PublishSubject<Bool> = PublishSubject()
    public let error : PublishSubject<SearchError> = PublishSubject()
    
    private let disposable = DisposeBag()
    
    
    public func requestData(query : String){
        self.loading.onNext(true)
        APIManager.sharedManager.fetchSearchTrack(
            onSuccess: {(track : [Track]) in
                self.loading.onNext(false)
                self.tracks.onNext(track)
        },
            onError: { [weak self](error: String) in
                guard let self = self else { return }
                self.loading.onNext(false)
                print("An error occured while get tracks \(error)")
        })
    }
}

//
//  Image.swift
//  DigiPay
//
//  Created by Fateme Mohammadi on 11/2/19.
//  Copyright © 2019 Fateme Mohammadi. All rights reserved.
//

import ObjectMapper

public final class Image: NSObject, Mappable {
    public init?(map: Map) {
        
    }
    
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
     private struct SerializationKeys {
      static let url = "url"
      static let height = "height"
      static let width = "width"

    }
    
    var url: String?
    var height: Int?
    var width: Int?
    
    init(url: String, height: Int,width: Int){
        self.url = url
        self.height = height
        self.width = width
    }
    
    public func mapping(map: Map) {
      url <- map[SerializationKeys.url]
      height <- map[SerializationKeys.height]
      width <- map[SerializationKeys.width]
    }

    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
      var dictionary: [String: Any] = [:]
      if let value = url { dictionary[SerializationKeys.url] = value }
      if let value = height { dictionary[SerializationKeys.height] = value }
      if let value = width { dictionary[SerializationKeys.width] = value }
      return dictionary
    }
}

//
//  Track.swift
//  Storm
//
//  Created by Mohammad Zakizadeh on 7/17/18.
//  Copyright © 2018 Storm. All rights reserved.
//

import Foundation
import ObjectMapper

public final class Track: NSObject, Mappable {
    public init?(map: Map) {
        
    }
    
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
     private struct SerializationKeys {
      static let id = "id"
      static let name = "name"
      static let popularity = "popularity"
      static let type = "type"
      static let images = "images"
    }
    
    var id, name, popularity, type: String?
    var images: [Image]?
    
    init(id: String, name: String, popularity: String, type: String,images: [Image] ){
        self.id = id
        self.name = name
        self.popularity = popularity
        self.type = type
        self.images = images
    }
    
    public func mapping(map: Map) {
      id <- map[SerializationKeys.id]
      name <- map[SerializationKeys.name]
      popularity <- map[SerializationKeys.popularity]
      type <- map[SerializationKeys.type]
      images <- map[SerializationKeys.images]
    }

    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
      var dictionary: [String: Any] = [:]
      if let value = id { dictionary[SerializationKeys.id] = value }
      if let value = name { dictionary[SerializationKeys.name] = value }
      if let value = popularity { dictionary[SerializationKeys.popularity] = value }
      if let value = type { dictionary[SerializationKeys.type] = value }
      if let value = images { dictionary[SerializationKeys.images] = value.map { $0.dictionaryRepresentation() } }
      return dictionary
    }
}

//
//  ViewController.swift
//  DigiPay
//
//  Created by Fateme Mohammadi on 11/1/19.
//  Copyright © 2019 Fateme Mohammadi. All rights reserved.
//

import UIKit
import SpotifyLogin

class ViewController: UIViewController {

    @IBOutlet weak var loggedInStackView: UIStackView!

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SpotifyLogin.shared.getAccessToken { [weak self] (token, error) in
            self?.loggedInStackView.alpha = (error == nil) ? 1.0 : 0.0
            if error != nil, token == nil {
                self?.showLoginFlow()
            }else{
                UserDefaults.standard.set(token, forKey: UserDefaultsKeys.userToken)
                self?.showTracksPage()
            }
        }
    }

    func showLoginFlow() {
        self.performSegue(withIdentifier: "home_to_login", sender: self)
    }
    
    func showTracksPage() {
        self.performSegue(withIdentifier: "tracks", sender: self)
    }

    @IBAction func didTapLogOut(_ sender: Any) {
        SpotifyLogin.shared.logout()
        self.loggedInStackView.alpha = 0.0
        self.showLoginFlow()
    }

}


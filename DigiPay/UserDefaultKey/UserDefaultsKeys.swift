//
//  UserDefaultsKeys.swift
//  DigiPay
//
//  Created by Fateme Mohammadi on 11/2/19.
//  Copyright © 2019 Fateme Mohammadi. All rights reserved.
//

import Foundation

struct UserDefaultsKeys {
    
    // User
    static let userToken = "UserDefaultsKey_UserToken"

}
